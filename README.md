# PFASDR.Mesa.BaseImage

A Docker base image common to all TensorFlow applications of PFASDR.

## Status

-   [![pipeline status](https://gitlab.com/pfasdr/mesa/pfasdr_mesa_baseimage/badges/master/pipeline.svg)](https://gitlab.com/pfasdr/mesa/pfasdr_mesa_baseimage/-/commits/master)
-   [![coverage report](https://gitlab.com/pfasdr/mesa/pfasdr_mesa_baseimage/badges/master/coverage.svg)](https://gitlab.com/pfasdr/mesa/pfasdr_mesa_baseimage/-/commits/master)
